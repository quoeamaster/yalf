// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// YALF - Yet.Another.Logging.Framework
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package yalf

import (
	"bytes"
	"fmt"
	"io"
	"time"
)

type IStream interface {
	// Log - simplest way to log a message to its designated output stream.
	Log(msg string) string

	// LogByModel - log the message to its designated output stream, all required attributes are provided by [LogModel].
	LogByModel(model LogModel) string

	// declare the Close() error func.
	io.Closer
}

// LogModel - a model declaring the attributes for logging.
type LogModel struct {
	// expected log level to log out the message
	Level             string
	Message           string
	FuncName          string
	NeedTimestamp     bool
	TimestampTemplate string
}

// ---------- stdOutStream >> log to stdout ---------- //

// stdOutStream - log to standard out.
type stdOutStream struct {
	streamName string
}

// NewStdOutStream - create an instance of [stdOutStream].
func NewStdOutStream(name ...string) (o IStream) {
	_o := new(stdOutStream)
	_o.streamName = StreamStdOut
	if len(name) > 0 && name[0] != "" {
		_o.streamName = name[0]
	}
	// return as an [IStream] instead of [stcOutStream].
	o = _o
	return
}

func (o *stdOutStream) LogByModel(model LogModel) string {
	// level check
	if !NewLogFactory().IsLoggable(o.streamName, model.Level) {
		return ""
	}
	// can log...
	_msg := o.formatMsg(model)
	fmt.Println(_msg)

	return _msg
}

func (o *stdOutStream) Log(msg string) string {
	_model := LogModel{
		Level:         LogLevelDebug,
		Message:       msg,
		NeedTimestamp: true,
	}
	return o.LogByModel(_model)
}

func (o *stdOutStream) Close() (e error) {
	// anything to housekeep?
	return
}

func (o *stdOutStream) formatMsg(model LogModel) (s string) {
	_sb := bytes.NewBuffer(nil)
	// [timestamp][level{4}] [funcName]msg
	if model.NeedTimestamp {
		if model.TimestampTemplate != "" {
			_sb.WriteString(fmt.Sprintf("[%v]", time.Now().Format(model.TimestampTemplate)))
		} else {
			_sb.WriteString(fmt.Sprintf("[%v]", time.Now().Format(time.RFC3339)))
		}
	}
	if model.Level != "" {
		if len(model.Level) > 4 {
			_sb.WriteString(fmt.Sprintf("[%v] ", model.Level[0:4]))
		} else {
			_sb.WriteString(fmt.Sprintf("[%-4v] ", model.Level))
		}
	}
	if model.FuncName != "" {
		_sb.WriteString(fmt.Sprintf("[%v]%v", model.FuncName, model.Message))
	} else {
		_sb.WriteString(fmt.Sprintf("%v", model.Message))
	}
	s = _sb.String()
	return
}
