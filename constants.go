// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// YALF - Yet.Another.Logging.Framework
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package yalf

const (
	// the log level; default is DEBUG
	EnvVarLogLevel string = "YALF_LOG_LV"

	LogLevelDebug string = "DEBUG"
	LogLevelInfo  string = "INFO"
	LogLevelWarn  string = "WARN"
	LogLevelError string = "ERROR"

	// the config file location, default is ./yalf.toml
	EnvVarConfig      string = "YALF_CONF"
	defaultConfigFile string = "./yalf.toml"

	ConfigLogThreshold     string = "logThreshold"
	ConfigLogType          string = "type"
	defaultRollingFilename string = "log"
	defaultRollingSize     int    = 100000000 // 100MB

	ConfigRollingFileFolder       string = "folder"
	ConfigRollingFilename         string = "filename"
	ConfigRollingFileCriteria     string = "criteria"
	ConfigRollingFileCriteriaSize string = "size"
	ConfigRollingFileSizeUnitKB   string = "KB"
	ConfigRollingFileSizeUnitMB   string = "MB"
	ConfigRollingFileSizeUnitGB   string = "GB"

	// supported IStream types
	StreamStdOut      string = "stdout"
	StreamRollingFile string = "rolling"
)
