// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// YALF - Yet.Another.Logging.Framework
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package yalf

import (
	"fmt"
	"os"
	"strconv"
	"sync"

	toml "github.com/pelletier/go-toml"
)

// ILogFactory - a Factory for creating [IStream] instances. The simplest [IStream] would be the [StdOutStream].
type ILogFactory interface {
	// Config - config the factory by any means, a [meta] is provided for the configuration.
	Config(meta interface{}) (i ILogFactory)

	// GetStream - return the [IStream] instance based on the provided [name].
	// If no such instance available, a default [IStream] would be returned.
	GetStream(name string) (o IStream)

	// IsLoggable - compare whether the provided stream-name vs level is loggable or not.
	// Take an example if the provided level is INFO, but through the configuration data
	// the provided IStream's minimal logging level is WARN, then this message should NOT be logged out.
	IsLoggable(name string, level string) bool
}

var singletonInstance ILogFactory
var lock sync.Mutex

// NewLogFactory - singleton function to return the ONLY ILogFactory instance.
func NewLogFactory() (o ILogFactory) {
	lock.Lock()
	defer lock.Unlock()

	if singletonInstance == nil {
		singletonInstance = newLogFactoryImpl()
	}
	return singletonInstance
}

// logFactoryImpl - an implementation for ILogFactory.
type logFactoryImpl struct {
	// level - general log level (if no complains, would use this value as the log level)
	level int
	// config object for various logger based on logger-name.
	// nil check is a MUST
	cfgTree *toml.Tree
	// default IStream for logging
	defaultStream IStream
}

// create an instance of [logFactoryImpl].
func newLogFactoryImpl() ILogFactory {
	_o := new(logFactoryImpl)

	// get the GENERAL log level from env var
	_lv := os.Getenv(EnvVarLogLevel)
	switch _lv {
	case LogLevelDebug:
		_o.level = 0
	case LogLevelInfo:
		_o.level = 1
	case LogLevelWarn:
		_o.level = 2
	case LogLevelError:
		_o.level = 3
	default:
		// default to DEBUG
		_o.level = 0
	}
	// default IStream - stdoutStream
	_o.defaultStream = NewStdOutStream()

	return _o
}

func (o *logFactoryImpl) Config(meta interface{}) (i ILogFactory) {
	// config starts here... too
	_cfgFile := os.Getenv(EnvVarConfig)
	if _cfgFile == "" {
		_cfgFile = defaultConfigFile
	}
	if _cTree, e := toml.LoadFile(_cfgFile); e != nil {
		fmt.Printf("[yalf][newLogFactoryImpl] failed to load the toml file, reason: [%v]\n", e)
	} else {
		o.cfgTree = _cTree
	}
	return o
}

func (o *logFactoryImpl) GetStream(name string) (i IStream) {
	// kind of hard-code... always return the default-stream
	if o.cfgTree == nil {
		i = o.defaultStream
		return
	}
	// try to get the IStream based on the [name]
	if _c := o.cfgTree.Get(name); _c == nil {
		// [debug]
		fmt.Printf("## %v IStream not found, default IStream is returned [%v]~\n", name, o.defaultStream)
		i = o.defaultStream
	} else {
		// logger / IStream name could be any arbitary value...
		// instead check the "type" attribute
		_cTree := _c.(*toml.Tree)
		if _streamType := _cTree.Get(ConfigLogType); _streamType == nil {
			// [debug]
			fmt.Printf("## %v IStream config found BUT missing 'type' value, default IStream is returned [%v]~\n", name, o.defaultStream)
			i = o.defaultStream
			return
		} else {
			_typeValue := _streamType.(string)
			switch _typeValue {
			case StreamStdOut:
				return NewStdOutStream(name)
			case StreamRollingFile:
				_folder := _cTree.Get(ConfigRollingFileFolder).(string)
				_filename := _cTree.Get(ConfigRollingFilename).(string)
				//_criteria := o.cfgTree.Get(fmt.Sprintf("%v.%v", name, ConfigRollingFileCriteria)).(*toml.Tree)
				_size := _cTree.Get(ConfigRollingFileCriteriaSize).(string)

				// convert the 10KB to 100000 bytes
				_iSize := ConvertStringSizeToInt(_size)
				// [debug]
				fmt.Printf("[yalf][newLogFactoryImpl] rollingStream {%v}, name {%v}, folder/file {%v - %v}\n", StreamRollingFile, name, _folder, _filename)

				return NewRollingFileStream(_folder, _filename, _iSize, name)

			// TODO: add back the other valid IStream(s) too...
			default:
				// no matter what... at least return the default IStream.
				return o.defaultStream
			}
		}
	}
	return
}

// ConvertStringSizeToInt - convert the provided [byteSize] to the number of bytes.
func ConvertStringSizeToInt(byteSize string) (size int) {
	// 100MB
	size = defaultRollingSize

	if len(byteSize) > 2 {
		// extract the last 2 characters...
		_unit := byteSize[len(byteSize)-2:]
		_qty, _e := strconv.Atoi(byteSize[0 : len(byteSize)-2])
		if _e != nil {
			// use default rolling size 100MB
			fmt.Printf("[yalf][ConvertStringSizeToInt] failed to convert [%v], reason: [%v], default value [%v] returned instead.\n", byteSize, _e, defaultRollingSize)
			return
		}
		switch _unit {
		case ConfigRollingFileSizeUnitKB:
			size = 1000 * _qty
		case ConfigRollingFileSizeUnitMB:
			size = 1000000 * _qty
		case ConfigRollingFileSizeUnitGB:
			size = 1000000000 * _qty
		default:
			size = defaultRollingSize
		}
	}
	return
}

func (o *logFactoryImpl) IsLoggable(name string, level string) bool {
	if o.cfgTree == nil {
		// [debug]
		//fmt.Printf("***** nil\n")
		return false
	}
	// [debug]
	//fmt.Printf("### %v \n[%v]:%v \n", o.cfgTree, name, o.cfgTree.Get(name))
	defer func() bool {
		if r := recover(); r != nil {
			fmt.Printf("[yalf][IsLoggable] failed to load config based on [%v], reason: %v\n", name, r)
		}
		// no matter what... should NOT log
		return false
	}()
	// check with the config object to determine whether the name-level pair is valid to log down the message.
	if _c := o.cfgTree.Get(name); _c != nil {
		_tree := _c.(*toml.Tree)
		// [lesson] - if the config for an IStream misses the logThreshold, the IStream would NEVER be able to log message(s)
		_logThreshold := 999
		if _value := _tree.Get(ConfigLogThreshold); _value != nil {
			_logThreshold = o.convertLogLevelStringToNumber(_value.(string))
		}
		_providedLevel := o.convertLogLevelStringToNumber(level)
		// make sure the providedLevel is a "valid" value, 999 here should be converted to -1
		// (making sure it could not be logged)
		if _providedLevel == 999 {
			_providedLevel = -1
		}
		//[debug]
		//fmt.Printf("** config.threshold:%v vs request loglevel:%v\n", _logThreshold, _providedLevel)

		// compare the logThreshold (within the config file) vs the provided/expcted level
		// if the expected level is higher or equals to the configured level, ok to log
		if _providedLevel >= _logThreshold {
			return true
		}
	}
	return false
}

// a conversion between the log (string) to a number.
func (o *logFactoryImpl) convertLogLevelStringToNumber(lv string) int {
	switch lv {
	case LogLevelDebug:
		return 0
	case LogLevelInfo:
		return 1
	case LogLevelWarn:
		return 2
	case LogLevelError:
		return 3
	default:
		// mean NEVER possible to log out anytihng
		return 999
	}
}
