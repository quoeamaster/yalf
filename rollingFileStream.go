// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// YALF - Yet.Another.Logging.Framework
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package yalf

import (
	"bytes"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"time"
)

// needs to be singleton... to prevent multiple instances trying to open the same log file...
var singletonRFLog IStream
var rfLock sync.Mutex

type rollingFileStream struct {
	// name - actual name / ID on the config toml.
	name string
	// targetFolder - where the log files are located
	targetFolder string
	// targetFile - the actual file name for logging
	// - append a date
	// - append a rolling number
	// e.g. ./logs/transactions-2022-01-02.0
	targetFile string
	// rollingSize - the criterion size for rolling
	rollingSize int

	// lastRolledTime - the time when the file was rolled.
	lastRolledTime time.Time

	fileHandle *os.File
}

// NewRollingFileStream - return the singleton instance of the [RollingFileStream].
func NewRollingFileStream(targetFolder, targetFile string, size int, name ...string) (o IStream) {
	rfLock.Lock()
	defer rfLock.Unlock()

	if singletonRFLog == nil {
		_o := new(rollingFileStream)
		_o.name = StreamRollingFile
		if len(name) > 0 && name[0] != "" {
			_o.name = name[0]
		}
		// folder and file...
		_o.targetFolder = targetFolder
		if targetFolder == "" {
			_o.targetFolder = "./"
		} else {
			_o.targetFolder = appendPathSeparator(_o.targetFolder)
		}
		// file...
		_o.targetFile = defaultConfigFile
		if targetFile != "" {
			_o.targetFile = targetFile
		}
		// create the missing file and folders if missing
		if e := createLogFolder(_o.targetFolder); e != nil {
			panic(fmt.Errorf("[yalf][NewRollingFileStream] failed to create the log folder [%v], reason: [%v]", _o.targetFolder, e))
		}
		// set the log file handle
		_o.setLogFileHandle()
		// rolling size
		_o.rollingSize = defaultRollingSize
		if size > 0 {
			_o.rollingSize = size
		}
		// last rolled time
		_o.lastRolledTime = truncateToNearestDate(time.Now())

		singletonRFLog = _o
	}
	return singletonRFLog
}

// truncateToNearestDate - truncate to the nearest Date.
func truncateToNearestDate(t time.Time) (t1 time.Time) {
	t1 = t
	if t.UnixNano() <= 0 {
		t1 = time.Now()
	}
	// truncate
	t1 = time.Date(t1.Year(), t1.Month(), t1.Day(), 0, 0, 0, 0, time.Local)
	return
}

func (o *rollingFileStream) Log(msg string) (s string) {
	_m := LogModel{
		Level:         LogLevelDebug,
		Message:       msg,
		NeedTimestamp: true,
	}
	return o.LogByModel(_m)
}

func (o *rollingFileStream) LogByModel(model LogModel) (s string) {
	// loggable?
	if !NewLogFactory().IsLoggable(o.name, model.Level) {
		return ""
	}
	// check rolling criteria before logging
	o.rolling()

	_msg := o.formatMsg(model)
	if _, e := o.fileHandle.WriteString(fmt.Sprintf("%v\n", _msg)); e != nil {
		fmt.Printf("[yalf] failed to log, reason: %v\n", e)
	}
	return _msg
}

func (o *rollingFileStream) Close() (e error) {
	defer func() {
		// nil the instance for re-configure again
		singletonRFLog = nil
	}()
	// close the file handler...
	if o.fileHandle != nil {
		return o.fileHandle.Close()
	}
	return
}

func (o *rollingFileStream) formatMsg(model LogModel) (s string) {
	_sb := bytes.NewBuffer(nil)
	// [timestamp][level{4}] [funcName]msg
	if model.NeedTimestamp {
		if model.TimestampTemplate != "" {
			_sb.WriteString(fmt.Sprintf("[%v]", time.Now().Format(model.TimestampTemplate)))
		} else {
			_sb.WriteString(fmt.Sprintf("[%v]", time.Now().Format(time.RFC3339)))
		}
	}
	if model.Level != "" {
		if len(model.Level) > 4 {
			_sb.WriteString(fmt.Sprintf("[%v] ", model.Level[0:4]))
		} else {
			_sb.WriteString(fmt.Sprintf("[%-4v] ", model.Level))
		}
	}
	if model.FuncName != "" {
		_sb.WriteString(fmt.Sprintf("[%v]%v", model.FuncName, model.Message))
	} else {
		_sb.WriteString(fmt.Sprintf("%v", model.Message))
	}
	s = _sb.String()
	return
}

func (o *rollingFileStream) rolling() {
	// rolling criteria
	// 1. a day passed (e.g. 2022-01-01 -> 2022-01-02) -> fileInfo.ModTime() vs actual time.Time.Now()
	// 2. the file size exceeds the configured criteria (e.g. 100Mb)
	// [roll-procedure]
	// a. close existing file-handle
	// b. rename the root log file to the log{-yyyy-mm-dd - 1}.{running-num}
	// c. create the root log file and set the handle.
	// d. update the lastRollingTime as well to the new date

	_target := fmt.Sprintf("%v%v", o.targetFolder, o.targetFile)
	// [debug]
	fmt.Printf("# target file for checking: %v\n", _target)

	if _info, _e := os.Stat(_target); _e != nil {
		fmt.Printf("[yalf][rollingFileStream] failed to check rollup file timestamp, reason: [%v]\n", _e)
	} else {
		// rolling criteria
		// 1. a day passed (e.g. 2022-01-01 -> 2022-01-02) -> fileInfo.ModTime() vs actual time.Time.Now()
		// 2. the file size exceeds the configured criteria (e.g. 100Mb)
		_needToRoll := false
		_now := truncateToNearestDate(time.Now())
		if _now.UnixNano() >= o.lastRolledTime.Add(time.Hour*24).UnixNano() {
			fmt.Printf("# rolled becoz time exceed\n")
			_needToRoll = true
		} else if _info.Size() >= int64(o.rollingSize) {
			fmt.Printf("# rolled becoz size exceeds [%v] [%v]\n", _info.Size(), o.rollingSize)
			_needToRoll = true
		}
		// [debug]
		/*
			fmt.Printf("# timestamp involved: %v: %v\n", _info.ModTime(), _info.ModTime().UnixNano())
			fmt.Printf("# timestamp involved: 1 day from lastRolled -> %v: now -> %v\n", o.lastRolledTime.Add(time.Hour*24).UnixNano(), _now.UnixNano())
			fmt.Printf("# size involved: %v: %v\n", _info.Size(), o.rollingSize)
			fmt.Printf("# need roll? %v\n", _needToRoll)
		*/
		// [roll-procedure]
		if _needToRoll {
			rfLock.Lock()
			defer rfLock.Unlock()

			// a. close existing file-handle
			if _e := o.fileHandle.Close(); _e != nil {
				//fmt.Printf("[yalf][rollingFileStream] failed to close log file's handle, reason: [%v]\n", _e)
				panic(fmt.Errorf("[yalf][rollingFileStream] failed to close log file's handle, reason: [%v]", _e))
			}
			// b. rename the root log file to the log{-yyyy-mm-dd - 1}.{running-num}
			if _e := o.rollTheFile(); _e != nil {
				panic(fmt.Errorf("[yalf][rollingFileStream] failed to roll the root log file, reason: [%v]", _e))
			}
			// c. create the root log file and set the handle.
			o.setLogFileHandle()

			// d. update the lastRollingTime as well to the new date
			o.lastRolledTime = _now
		}
	}
	// [debug]
	//fmt.Printf("# rolling size: %v\n", o.rollingSize)
}

// rollTheFile - really roll the root log file to the corresponding historical log file.
func (o *rollingFileStream) rollTheFile() (e error) {
	_oFile := fmt.Sprintf("%v%v", o.targetFolder, o.targetFile)
	_logs := make([]string, 0)
	filepath.Walk(o.targetFolder, func(path string, info fs.FileInfo, err error) (e2 error) {
		if strings.Contains(info.Name(), o.targetFile) {
			// is it the same _oFile?
			if !strings.HasSuffix(path, o.targetFile) {
				_logs = append(_logs, path)
			}
		}
		return
	})
	// len(_logs) == 0 -> no rolled file(s) yet; len(_logs) > 0 -> has at least 1 rolled file
	_yesterday := truncateToNearestDate(time.Now().Add(-1 * time.Hour * 24))
	_nFile := ""
	if len(_logs) == 0 {
		_nFile = fmt.Sprintf("%v%v.%v.000", o.targetFolder, o.targetFile, _yesterday.Format("2006-01-02"))
	} else {
		_nFile = fmt.Sprintf("%v%v.%v.%03v", o.targetFolder, o.targetFile, _yesterday.Format("2006-01-02"), len(_logs))
	}
	// [debug]
	fmt.Printf("# oFile: %v -> %v\n", _oFile, _nFile)
	// rename / move
	e = os.Rename(_oFile, _nFile)
	return
}

// setLogFileHandle - call this function when required to create the root log file and set the associated [handle].
func (o *rollingFileStream) setLogFileHandle() {
	// [lesson] - the target file MUST always be the same root file (without the running number appended).
	//  e.g. app.log; so if rolling required, then
	//  - the root app.log -> app{-yyyy-mm-dd - 1}.log.0;
	//  - and a brand "new" app.log is created to receive new logs.
	_logFile := fmt.Sprintf("%v%v", o.targetFolder, o.targetFile)
	if _handle, e := os.OpenFile(_logFile, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0755); e != nil || _handle == nil {
		panic(fmt.Errorf("[yalf][NewRollingFileStream] failed to get a file-handle based on the given file [%v], reason: [%v][%v]", _logFile, e, _handle))
	} else {
		o.fileHandle = _handle
	}
}

// appendPathSeparator - add "/" to the [path] if not found.
func appendPathSeparator(path string) (s string) {
	s = path
	if len(path) > 0 && path[len(path)-1:] != string(os.PathSeparator) {
		s = fmt.Sprintf("%v%v", path, string(os.PathSeparator))
	}
	return
}

// xtractRollingNumber - return the rolling number of the given file.
// - if ends with a valid number (e.g. logs.2); then the number would be returned (i.e. 2) -> logs.2
// - if not ending with a valid number (e.g. logs); then a hard "0" is returned, treated as the first rolling file -> logs.0
// - if the file ends with a non-number part (e.g. logs.a); then a hard "0" is returned, treated as the first rolling file -> logs.a.0
// [deprecated]
/*
func xtractRollingNumber(path string) int {
	_parts := strings.Split(path, ".")
	if len(_parts) > 0 {
		_i := _parts[len(_parts)-1]
		if i, _e := strconv.Atoi(_i); _e != nil {
			// set to "0"
			return 0
		} else {
			return i
		}
	}
	return 0
}
*/

func createLogFolder(target string) (e error) {
	return os.MkdirAll(target, 0755)
}
