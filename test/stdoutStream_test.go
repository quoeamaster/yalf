// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// YALF - Yet.Another.Logging.Framework
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package test

import (
	"fmt"
	"os"
	"testing"

	y "gitlab.com/quoeamaster/yalf"
)

func Test_stdoutStream_0(t *testing.T) {
	fmt.Println(prepareTitleStdoutStreamTest("Test_stdoutStream_0",
		`demo on stdoutStream basic logging features - yalf.toml NOT found.`))

	_stream := y.NewLogFactory().Config(nil).GetStream(y.StreamStdOut)

	if _stream == nil {
		t.Fatalf("0. expect non nil (always have a default StdoutStream")
	}
	_stream.Log("a message that should not be logged, reason: no yalf.toml found and isLoggable == false")

}

func Test_stdoutStream_1(t *testing.T) {
	fmt.Println(prepareTitleStdoutStreamTest("Test_stdoutStream_1",
		`demo on stdoutStream basic logging features - yalf.toml available through the ENV var.`))

	os.Setenv(y.EnvVarConfig, "./data/yalf.toml")
	defer os.Unsetenv(y.EnvVarConfig)

	// [lesson] - normal scenario is... the Config(interface{}) is called only once in the lifecycle of the application
	// 	for unit test purposes, it is ok to call the Config() more than once...
	_stream := y.NewLogFactory().Config(nil).GetStream(y.StreamStdOut)
	if _stream == nil {
		t.Fatalf("0. expect non nil (always have a default StdoutStream")
	}
	_stream.Log("a message shoul NOT be logged since the config file states the threshold to be INFO.")
	_stream.LogByModel(y.LogModel{
		Message:       "this message is logged since INFO level",
		Level:         y.LogLevelInfo,
		NeedTimestamp: true,
		FuncName:      "Test_stdoutStream_1",
	})
	_stream.LogByModel(y.LogModel{
		Message:       "this message is logged since **ERROR** level",
		Level:         y.LogLevelError,
		NeedTimestamp: true,
		FuncName:      "Test_stdoutStream_1",
	})
}

func Test_stdoutStream_2_e(t *testing.T) {
	fmt.Println(prepareTitleStdoutStreamTest("Test_stdoutStream_2_e",
		"demo on loading a toml but mixssing the required config..."))

	defer os.Unsetenv(y.EnvVarConfig)
	os.Setenv(y.EnvVarConfig, "./data/yalf_missing_type.toml")
	var _stream y.IStream

	// missing the provided Stream-name
	_stream = y.NewLogFactory().Config(nil).GetStream("unknown")
	if _stream == nil {
		t.Fatalf("a0. expect non nil (always have a default StdoutStream")
	}

	// missng "type"
	_stream = y.NewLogFactory().Config(nil).GetStream("myStandardOut")
	if _stream == nil {
		t.Fatalf("b0. expect non nil (always have a default StdoutStream")
	}

	// missing "logThreshold"
	_stream = y.NewLogFactory().Config(nil).GetStream("missingThreshold")
	if _stream == nil {
		t.Fatalf("c0. expect non nil (always have a default StdoutStream")
	}
	_stream.Log("c1. should not have this message loggable... since no LogThreshold configured")
}

func prepareTitleStdoutStreamTest(funcName string, explain string) string {
	_t := titleStruct{
		file:     "stdoutStream_test",
		function: funcName,
		explain:  explain,
	}
	return _t.String()
}
