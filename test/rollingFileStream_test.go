// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// YALF - Yet.Another.Logging.Framework
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package test

import (
	"fmt"
	"io/fs"
	"math/rand"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"testing"
	"time"

	y "gitlab.com/quoeamaster/yalf"
)

const (
	simpleLogLine      string = "this is a message logged to the file..."
	logLineOver1KB     string = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Senectus et netus et malesuada fames ac turpis egestas. Id faucibus nisl tincidunt eget. Aliquam ut porttitor leo a diam sollicitudin tempor id. At urna condimentum mattis pellentesque. In eu mi bibendum neque egestas congue quisque egestas. Cras sed felis eget velit aliquet sagittis. Diam maecenas sed enim ut sem viverra aliquet. Morbi tempus iaculis urna id volutpat lacus laoreet. Felis donec et odio pellentesque diam volutpat commodo sed. Ultrices sagittis orci a scelerisque purus. Augue lacus viverra vitae congue eu consequat. Vitae aliquet nec ullamcorper sit amet. Neque laoreet suspendisse interdum consectetur. Eget arcu dictum varius duis at. Bibendum ut tristique et egestas quis ipsum suspendisse ultrices. Mattis vulputate enim nulla aliquet porttitor. Leo duis ut diam quam nulla porttitor. Enim sed faucibus turpis in eu mi bibendum neque egestas. Ridiculus mus mauris vitae ultricies leo. Tristique senectus et netus et malesuada. Quis lectus nulla at volutpat diam. Nunc mi ipsum faucibus vitae. Dolor magna eget est lorem ipsum dolor. Netus et malesuada fames ac turpis egestas integer eget. Condimentum vitae sapien pellentesque habitant morbi tristique senectus et netus. Sodales neque sodales ut etiam sit amet nisl. Ante metus dictum at tempor commodo ullamcorper. Condimentum lacinia quis vel eros donec ac odio. Consectetur adipiscing elit ut aliquam purus sit amet luctus venenatis."
	logLineLessThan1KB string = "Tempor commodo ullamcorper a lacus vestibulum sed arcu non. Sagittis purus sit amet volutpat consequat mauris nunc. Viverra aliquet eget sit amet tellus. Amet purus gravida quis blandit turpis. Donec pretium vulputate sapien nec. Risus at ultrices mi tempus. Senectus et netus et malesuada fames ac turpis egestas. Libero justo laoreet sit amet cursus sit. Imperdiet dui accumsan sit amet nulla facilisi morbi tempus. Id donec ultrices tincidunt arcu non sodales neque sodales ut. Orci nulla pellentesque dignissim enim. Ullamcorper sit amet risus nullam. Enim neque volutpat ac tincidunt."
)

func Test_rollingFileStream_1(t *testing.T) {
	fmt.Println(prepareTitleRollingFileStreamTest("Test_rollingFileStream_1", `testing on file logging (no test against rolling yet)`))

	os.Setenv(y.EnvVarConfig, "./data/yalf.toml")
	defer os.Unsetenv(y.EnvVarConfig)

	// 0. cleanup from last unit test
	if e := deleteFiles("file", "./data/"); e != nil {
		t.Fatalf("0a. unexpected error when cleaning up file.log*, reason: [%v]", e)
	}

	_stream := y.NewLogFactory().Config(nil).GetStream("rolling_demo")
	if _stream == nil {
		t.Fatalf("0b. expect non nil (always have a default StdoutStream")
	}
	v := _stream.LogByModel(y.LogModel{
		Level:         y.LogLevelInfo,
		Message:       simpleLogLine,
		NeedTimestamp: true,
	})
	// v compare the content in the log file
	if v == "" {
		t.Fatalf("1. expect non empty logLine value, actual [%v]", v)
	}
	if !strings.Contains(v, simpleLogLine) {
		t.Fatalf("2. expect message '%v', actual [%v]", simpleLogLine, v)
	}
	// one more log line
	_stream.LogByModel(y.LogModel{
		Level:         y.LogLevelInfo,
		Message:       simpleLogLine,
		NeedTimestamp: true,
	})
	// check rolling , should NOT happened
	_fileList := getLogFiles("file", "./data/")
	if len(_fileList) != 1 {
		t.Fatalf("3. expect only 1 and ONLY 1 log file available... actual %v", _fileList)
	}
	fmt.Printf("[test][Test_rollingFileStream_1] fileList: %v\n", _fileList)

	// [debug] - testing on the %03v format
	/*
		fmt.Printf("# %v.%03v\n", time.Now().Format("2006-01-02"), 12)
		fmt.Printf("# %v.%03v\n", time.Now().Format("2006-01-02"), 8)
		fmt.Printf("# %v.%03v\n", time.Now().Format("2006-01-02"), 987)
	*/
	// close
	if _e := _stream.Close(); _e != nil {
		t.Fatalf("99. failed to close the stream, reason [%v]\n", _e)
	}
}

// test on rolling effect(s)
// log 5 lines
// 1. normal one line
// 2. over 1k line
// 3. less than 1k line (spawn a new file)
// 4. over 1k line
// 5. normal one line (spawn a new file)
func Test_rollingFileStream_2(t *testing.T) {
	fmt.Println(prepareTitleRollingFileStreamTest("Test_rollingFileStream_2", `testing on rolling on a 1KB limit)`))

	os.Setenv(y.EnvVarConfig, "./data/yalf.toml")
	defer os.Unsetenv(y.EnvVarConfig)

	// 0. cleanup from last unit test
	if e := deleteFiles("rolling", "./data/"); e != nil {
		t.Fatalf("0a. unexpected error when cleaning up rolling.log*, reason: [%v]", e)
	}

	_stream := y.NewLogFactory().Config(nil).GetStream("rolling_demo_2")
	if _stream == nil {
		t.Fatalf("0b. expect non nil (always have a default StdoutStream")
	}
	_stream.LogByModel(y.LogModel{
		Level:         y.LogLevelInfo,
		Message:       simpleLogLine,
		NeedTimestamp: true,
	}) // 74
	_stream.LogByModel(y.LogModel{
		Level:         y.LogLevelWarn,
		Message:       logLineOver1KB,
		NeedTimestamp: true,
	}) // 1537
	// should spawn a new file
	_stream.LogByModel(y.LogModel{
		Level:         y.LogLevelWarn,
		Message:       logLineLessThan1KB,
		NeedTimestamp: true,
	}) // 590
	_stream.LogByModel(y.LogModel{
		Level:         y.LogLevelWarn,
		Message:       logLineOver1KB,
		NeedTimestamp: true,
	}) // 1537
	// should again spawn another new file
	_stream.LogByModel(y.LogModel{
		Level:         y.LogLevelWarn,
		Message:       simpleLogLine,
		NeedTimestamp: true,
	}) // 74
	// get the files... for verification
	// yesterday
	_now := time.Now()
	_yesterday := time.Date(_now.Year(), _now.Month(), _now.Day(), 0, 0, 0, 0, time.Local).Add(-1 * time.Hour * 24)

	if _info, _e := os.Stat("./data/rolling.log"); _e != nil {
		t.Errorf("1a. failed to get the ./data/rolling.log handle, reason: [%v]\n", _e)
	} else {
		if !(_info.Size() >= 74 && _info.Size() < 80) {
			t.Errorf("1a-1. expect around 74 bytes of content in the ./data/rolling.log, actual [%v]\n", _info.Size())
		}
	}

	if _info, _e := os.Stat(fmt.Sprintf("./data/rolling.log.%v.000", _yesterday.Format("2006-01-02"))); _e != nil {
		t.Errorf("1b. failed to get the ./data/rolling.log.{yyyy-MM-dd -1}.000 handle, reason: [%v]\n", _e)
	} else {
		if !(_info.Size() >= 1645 && _info.Size() < 1650) {
			t.Errorf("1b-1. expect around 1611 bytes of content in the ./data/rolling.log.{yyyy-MM-dd -1}.001, actual [%v]\n", _info.Size())
		}
	}

	if _info, _e := os.Stat(fmt.Sprintf("./data/rolling.log.%v.001", _yesterday.Format("2006-01-02"))); _e != nil {
		t.Errorf("1c. failed to get the ./data/rolling.log.{yyyy-MM-dd -1}.001 handle, reason: [%v]\n", _e)
	} else {
		if !(_info.Size() >= 2195 && _info.Size() < 2200) {
			t.Errorf("1c-1. expect around 2127 bytes of content in the ./data/rolling.log.{yyyy-MM-dd -1}.001, actual [%v]\n", _info.Size())
		}
	}

	// close
	if _e := _stream.Close(); _e != nil {
		t.Fatalf("99. failed to close the stream, reason [%v]\n", _e)
	}
}

// getLogFiles - return the related log file(s) for verifying whether any rolling happened.
func getLogFiles(rootLogFilename string, folder string) (files []string) {
	files = make([]string, 0)
	filepath.Walk(folder, func(path string, info fs.FileInfo, err error) (e error) {
		if err != nil {
			return err
		}
		if !info.IsDir() && strings.Index(info.Name(), rootLogFilename) == 0 {
			files = append(files, path)
		}
		return
	})
	return
}

// TODO: parallel access on the same rollingFileStream

func Test_rollingFileStream_3(t *testing.T) {
	fmt.Println(prepareTitleRollingFileStreamTest("Test_rollingFileStream_3", `testing on rolling on a 1KB limit + concurrency`))

	os.Setenv(y.EnvVarConfig, "./data/yalf.toml")
	defer os.Unsetenv(y.EnvVarConfig)

	// 0. cleanup from last unit test
	if e := deleteFiles("rolling", "./data/"); e != nil {
		t.Fatalf("0a. unexpected error when cleaning up rolling.log*, reason: [%v]", e)
	}

	_stream := y.NewLogFactory().Config(nil).GetStream("rolling_demo_2")
	if _stream == nil {
		t.Fatalf("0b. expect non nil (always have a default StdoutStream")
	}

	_wg := sync.WaitGroup{}
	_wg.Add(5)
	// create 2 go-routine to log messages
	go func() {
		rand.Seed(time.Now().UnixMilli())
		_stream.LogByModel(y.LogModel{
			Level:         y.LogLevelError,
			Message:       simpleLogLine,
			FuncName:      "routine-1",
			NeedTimestamp: true,
		})
		time.Sleep(time.Second * time.Duration(rand.Intn(3)))
		_wg.Done()
		_stream.LogByModel(y.LogModel{
			Level:         y.LogLevelError,
			Message:       logLineOver1KB,
			FuncName:      "routine-1",
			NeedTimestamp: true,
		})
		time.Sleep(time.Second * time.Duration(rand.Intn(3)))
		_wg.Done()
		_stream.LogByModel(y.LogModel{
			Level:         y.LogLevelError,
			Message:       logLineLessThan1KB,
			FuncName:      "routine-1",
			NeedTimestamp: true,
		})
		time.Sleep(time.Second * time.Duration(rand.Intn(3)))
		_wg.Done()
	}()

	go func() {
		rand.Seed(time.Now().UnixMilli())
		_stream.LogByModel(y.LogModel{
			Level:         y.LogLevelError,
			Message:       logLineLessThan1KB,
			FuncName:      "routine-2",
			NeedTimestamp: true,
		})
		time.Sleep(time.Second * time.Duration(rand.Intn(3)))
		_wg.Done()
		_stream.LogByModel(y.LogModel{
			Level:         y.LogLevelError,
			Message:       simpleLogLine,
			FuncName:      "routine-2",
			NeedTimestamp: true,
		})
		time.Sleep(time.Second * time.Duration(rand.Intn(3)))
		_wg.Done()
	}()
	_wg.Wait()
	if _e := _stream.Close(); _e != nil {
		t.Fatalf("failed to close the rollingFileStream, reason [%v]", _e)
	}
}

// deleteFiles - remove log-files before running any test...
func deleteFiles(rootLogFilename string, folder string) (e error) {
	_fileList := getLogFiles(rootLogFilename, folder)
	for _, _file := range _fileList {
		e = os.Remove(_file)
		if e != nil {
			return
		}
	}
	return
}

func prepareTitleRollingFileStreamTest(funcName string, explain string) string {
	_t := titleStruct{
		file:     "rollingfilestream_test",
		function: funcName,
		explain:  explain,
	}
	return _t.String()
}
