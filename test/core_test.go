// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// YALF - Yet.Another.Logging.Framework
// Copyright (C) 2022 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
package test

import (
	"bytes"
	"fmt"
	"strings"
)

// titleStruct - a struct for printing the testing function's explanations.
type titleStruct struct {
	file     string
	function string
	explain  string
}

// String - print out the titleStruct.
func (o *titleStruct) String() string {
	_msg := bytes.NewBuffer(nil)
	_len := 80

	_msg.WriteString("\n")

	// upper / lower frame
	for i := 0; i < _len; i++ {
		_msg.WriteString("*")
	}
	_msg.WriteString("\n")
	_msg.WriteString(fmt.Sprintf("* %76v *\n", ""))

	// file
	_msg.WriteString(fmt.Sprintf("* %-10v: %-64v *\n", "file", o.file))
	// function
	_msg.WriteString(fmt.Sprintf("* %-10v: %-64v *\n", "function", o.function))
	// explain
	if o.explain != "" {
		_msg.WriteString(fmt.Sprintf("* %76v *\n", ""))
		_len2 := _len - 4
		// trim the explain into chunks of _len2
		_offset := 0
		_explainLen := len(o.explain)
		// [debug]
		//fmt.Printf("_len: %v, _len2: %v, _explainLeb: %v, _offset: %v\n", _len, _len2, _explainLen, _offset)
		for {
			// line is longer than _len2 (e.g. if total len = 80; _len2 = 80-4 = 76)
			if _offset+_len2 < _explainLen {
				_msg.WriteString(fmt.Sprintf("* %v *\n", strings.Trim(o.explain[_offset:_offset+_len2], " ")))
				_offset = _offset + _len2
			} else {
				// last line probably
				_msg.WriteString(fmt.Sprintf("* %-76v *\n", strings.Trim(o.explain[_offset:], " ")))
				break
			}
		}
	}

	_msg.WriteString(fmt.Sprintf("* %76v *\n", ""))
	for i := 0; i < _len; i++ {
		_msg.WriteString("*")
	}
	_msg.WriteString("\n")

	return _msg.String()
}
